const expect = require('chai').expect;

const { Builder, By, Key, WebElement, Browser } = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
require('geckodriver');

const BASE_URL = "https://niisku.lab.fi/~kardev/projects/color-picker";
const sleep = (seconds) => new Promise((resolve) => setTimeout(resolve, seconds * 1000));

describe('UI Tests', () => {
    /** @type {import('selenium-webdriver').ThenableWebDriver} */
    let driver = undefined;
    before(async () => {
        const options = new firefox.Options();
        options.setBinary("C:\\Program Files\\Mozilla Firefox\\firefox.exe");
        driver = await new Builder()
            .forBrowser(Browser.FIREFOX)
            .setFirefoxOptions(options)
            .build();
        await driver.get(BASE_URL);
        await sleep(1);
    });

    it('Can find all rgb sliders', async () => {
        const slider_r = await driver.findElement(By.id("slider-r"));
        const slider_g = await driver.findElement(By.id("slider-g"));
        const slider_b = await driver.findElement(By.id("slider-b"));
    });
    it('Can find all rgb inputs', async () => {
        const value_r = await driver.findElement(By.id("value-r"));
        const value_g = await driver.findElement(By.id("value-g"));
        const value_b = await driver.findElement(By.id("value-b"));
    });
    it('Can find hex value from "value-hex" input field', async () => {
        const hex = await driver.findElement(By.id("value-hex")).getAttribute('value');
        expect(hex).to.eq("#00ff00");
    });
    it('Can get and set "value-rgb" input field', async () => {
        const rgb = await driver.findElement(By.id("value-rgb"));
        let rgb_value = await rgb.getAttribute('value');
        expect(rgb_value).to.eq("rgb(0, 255, 0)");
        const keystroke_sequence = [];
        for (let i = 0; i < rgb_value.length; i++) {
            keystroke_sequence.push(Key.BACK_SPACE);
        }
        keystroke_sequence.push("rgb(255, 255, 0)");
        keystroke_sequence.push(Key.ENTER);
        await rgb.sendKeys(...keystroke_sequence);
        rgb_value = await rgb.getAttribute('value');
        expect(rgb_value).to.eq("rgb(255, 255, 0)");
        await sleep(1);
    });
    it('Can move sliders', async () => {
        const slider_r = await driver.findElement(By.id("slider-r"));
        const slider_g = await driver.findElement(By.id("slider-g"));
        const slider_b = await driver.findElement(By.id("slider-b"));
        await slider_r.sendKeys(Key.LEFT.repeat(300));
        await slider_g.sendKeys(Key.LEFT.repeat(300));
        await slider_b.sendKeys(Key.LEFT.repeat(300));
        await sleep(1);
    });
    it.skip('', async () => {});

    after(async () => {
        await driver.close();
    });
});

